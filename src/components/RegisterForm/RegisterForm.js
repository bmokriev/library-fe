import React from 'react';
import { ReactComponent as EyeIcon } from '../../assets/eyeIcon.svg';
import './registerForm.scss';
import axios from 'axios';
import { Link, useNavigate } from 'react-router-dom';
import { IoMdEye } from 'react-icons/io';

function RegisterForm() {
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState({ value: '', isVisable: false });
  const [passwordConfirm, setPasswordConfirm] = React.useState({
    value: '',
    isVisable: false,
  });
  const [dataIsValid, setDataIsValid] = React.useState(false);
  const navigate = useNavigate();

  React.useEffect(() => {
    if (
      username.length < 4 ||
      password.value.length < 6 ||
      passwordConfirm.value.length < 6
    ) {
      setDataIsValid(false);
      return;
    }

    if (password.value !== passwordConfirm.value) {
      setDataIsValid(false);
      return;
    }

    setDataIsValid(true);
  }, [username, password, passwordConfirm]);

  async function handleSubmit(ev) {
    ev.preventDefault();

    if (!dataIsValid) {
      return;
    }

    const url = 'https://books-library-dev.herokuapp.com/api/user/register';
    const body = JSON.stringify({
      username,
      password: password.value,
    });
    try {
      let res = await axios.post(url, body, {
        headers: { 'Content-Type': 'application/json' },
      });
      console.log(res.data);
      if (res.status === 201) {
        navigate('/login');
      }
    } catch (error) {
      console.log(error.response.data);
    }
  }

  return (
    <div id='register-form'>
      <h1>WELCOME TO THE BEST BOOK DATABASE!</h1>
      <h2>CREATE YOUR PROFILE</h2>
      <form onSubmit={handleSubmit}>
        <div className='form-element'>
          <label htmlFor='username'>Email</label>
          <input
            type='text'
            id='username'
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </div>
        <div className='form-element'>
          <label htmlFor='password'>Password</label>
          <div className='password-field'>
            <input
              type={password.isVisable ? 'input' : 'password'}
              id='password'
              value={password.value}
              onChange={(ev) =>
                setPassword((prev) => ({ ...prev, value: ev.target.value }))
              }
            />
            <IoMdEye
              className={`eye-icon ${password.isVisable ? 'active' : ''}`}
              onClick={() =>
                setPassword((prev) => ({
                  ...prev,
                  isVisable: !prev.isVisable,
                }))
              }
            />
          </div>
        </div>
        <div className='form-element'>
          <label htmlFor='password-confirm'>Repeat password</label>
          <div className='password-field'>
            <input
              type={passwordConfirm.isVisable ? 'input' : 'password'}
              id='password-confirm'
              value={passwordConfirm.value}
              onChange={(ev) =>
                setPasswordConfirm((prev) => ({ ...prev, value: ev.target.value }))
              }
            />
            <IoMdEye
              className={`eye-icon ${passwordConfirm.isVisable ? 'active' : ''}`}
              onClick={() =>
                setPasswordConfirm((prev) => ({
                  ...prev,
                  isVisable: !prev.isVisable,
                }))
              }
            />
          </div>
        </div>

        <button type='submit' className={dataIsValid ? 'active-btn' : ''}>
          SIGN UP
        </button>
      </form>
      <p id='alternative-access'>
        You have an account? <Link to='/login'>LOG IN HERE</Link>
      </p>
    </div>
  );
}

export default RegisterForm;
