import React, { useEffect, useState } from 'react';
import './appLayout.scss';
import { Link, Outlet, useNavigate } from 'react-router-dom';
import { IoCaretBack, IoMenuOutline, IoCloseOutline } from 'react-icons/io5';

import { ReactComponent as Logo } from '../../assets/logo.svg';
import { ReactComponent as ProfileIcon } from '../../assets/profile.svg';
import CustomLink from '../CustomLink/CustomLink';

function AppLayout({ detail = false }) {
  const [extendet, setExtendet] = useState(false);
  const [innerWidth, setInnerWidth] = useState(window.innerWidth);
  const navigate = useNavigate();

  //close dropdown on resize
  useEffect(() => {
    function handleResize() {
      setInnerWidth(window.innerWidth);

      if (innerWidth > 800) {
        setExtendet(false);
      }
    }

    window.addEventListener('resize', handleResize);

    handleResize();

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [innerWidth]);

  function logOut() {
    localStorage.removeItem('token');
    navigate('/login');
  }

  return (
    <div id='page'>
      <div id='nav-background'>
        <div className='container'>
          <div id='navbar'>
            {extendet ? (
              <IoCloseOutline
                className='ham-menu-icon'
                onClick={() => setExtendet((prev) => !prev)}
              />
            ) : (
              <IoMenuOutline
                className='ham-menu-icon'
                onClick={() => setExtendet((prev) => !prev)}
              />
            )}

            {detail ? (
              <Link to='/home'>
                <div className='back-btn'>
                  <IoCaretBack />
                  <span>Library</span>
                </div>
              </Link>
            ) : (
              <Logo id='logo' />
            )}

            <nav id='links'>
              <CustomLink to={'/home'}>Library</CustomLink>

              <CustomLink to={'/home/settings'}>Settings</CustomLink>
            </nav>
            <div className='edge-container'>
              <ProfileIcon id='profile' onClick={logOut} />
            </div>
          </div>
        </div>
        {extendet && (
          <nav id='links-ham'>
            <CustomLink to={'/home'}>Library</CustomLink>

            <CustomLink to={'/home/settings'}>Settings</CustomLink>
          </nav>
        )}
      </div>
      <Outlet style={{ 'z-index': 5 }} />
    </div>
  );
}

export default AppLayout;
