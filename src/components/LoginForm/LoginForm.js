import React from 'react';
import './loginForm.scss';
import axios from 'axios';
import { Link, useNavigate } from 'react-router-dom';
import { IoMdEye } from 'react-icons/io';

function LoginForm() {
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState({ value: '', isVisable: false });
  const [dataIsValid, setDataIsValid] = React.useState(false);

  const navigate = useNavigate();

  React.useEffect(() => {
    if (username.length < 4 || password.value.length < 6) {
      setDataIsValid(false);
      return;
    } else {
      setDataIsValid(true);
    }
  }, [username, password]);

  async function handleSubmit(ev) {
    ev.preventDefault();

    if (!dataIsValid) {
      return;
    }

    const url = 'https://books-library-dev.herokuapp.com/api/user/login';
    const body = JSON.stringify({ username, password: password.value });
    try {
      const res = await axios.post(url, body, {
        headers: { 'Content-Type': 'application/json' },
      });
      localStorage.setItem('token', res.data.token);
      navigate('/home');
    } catch (err) {
      console.log(err);
      alert('Wrong Credentials');
    }
  }

  return (
    <div id='login-form'>
      <h1>WELCOME BACK!</h1>
      <form onSubmit={handleSubmit}>
        <div className='form-element'>
          <label htmlFor='username'>Email</label>
          <input
            type='text'
            id='username'
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </div>
        <div className='form-element'>
          <label htmlFor='password'>Password</label>
          <div className='password-field'>
            <input
              type={password.isVisable ? 'input' : 'password'}
              id='password'
              value={password.value}
              onChange={(ev) =>
                setPassword((prev) => ({ ...prev, value: ev.target.value }))
              }
            />
            <IoMdEye
              id='eye-icon'
              className={password.isVisable ? 'active' : ''}
              onClick={() =>
                setPassword((prev) => ({
                  ...prev,
                  isVisable: !prev.isVisable,
                }))
              }
            />
          </div>
        </div>
        <a id='recover' href='#'>
          Recover password
        </a>

        <button
          type='submit'
          disabled={!dataIsValid}
          className={dataIsValid ? 'active-btn' : ''}
        >
          LOG IN
        </button>
      </form>
      <p id='alternative-access'>
        You don’t have an account? <Link to='/register'>SIGN UP HERE</Link>
      </p>
    </div>
  );
}

export default LoginForm;
