import axios from 'axios';
import React, { useState } from 'react';
import './homePage.scss';
import BookCard from '../BookCard/BookCard';
import { ReactComponent as SearchIcon } from '../../assets/search-icon.svg';

function HomePage() {
  const [books, setBooks] = useState([]);
  const [pattern, setPattern] = useState('');
  const [searchLabel, setSearchLabel] = useState('');

  //load all books
  React.useEffect(() => {
    getAllBooks();
  }, []);

  async function getAllBooks() {
    const token = localStorage.getItem('token');

    if (!token) {
      console.log('no token');
      return;
    }

    const url = 'https://books-library-dev.herokuapp.com/api/book';
    const header = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    };
    try {
      const res = await axios.get(url, header);
      setBooks(res.data);
      setSearchLabel('all books');
    } catch (error) {
      console.log(error);
    }
  }

  function handleSearch(ev) {
    ev.preventDefault();

    if (pattern !== '') {
      searchBooks();
    } else {
      getAllBooks();
    }
  }

  async function searchBooks() {
    const token = localStorage.getItem('token');

    if (!token) {
      console.log('no token');
      return;
    }

    const url = 'https://books-library-dev.herokuapp.com/api/book/search';
    const header = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    };
    const body = JSON.stringify({ pattern });
    try {
      const res = await axios.post(url, body, header);
      setBooks(res.data);
      setSearchLabel(`Results for: ${pattern}`);
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <div className='outlet-container'>
      <div className='content-container'>
        <div id='search-form'>
          <label htmlFor='search'>{searchLabel}</label>
          <div>
            <input
              type='text'
              id='search'
              placeholder='Search'
              value={pattern}
              onChange={(ev) => setPattern(ev.target.value)}
            />
            <SearchIcon onClick={handleSearch} id='search-icon' />
          </div>
        </div>
        <div id='book-grid'>
          {books.map((book) => (
            <BookCard key={book._id} {...book} />
          ))}
        </div>
      </div>
    </div>
  );
}

export default HomePage;
