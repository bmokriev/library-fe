import React, { useState } from 'react';
import axios from 'axios';
import { useLocation } from 'react-router-dom';
import './details.scss';
import { ipsum1, ipsum2 } from '../../assets/hpIpsum.js';

function Details() {
  const [bookData, setBookData] = useState({});
  const [loading, setLoading] = useState(true);
  const location = useLocation();

  React.useEffect(() => {
    getAllBooks();
  }, []);

  async function getAllBooks() {
    const token = localStorage.getItem('token');

    if (!token) {
      console.log('no token');
      return;
    }

    const url = `https://books-library-dev.herokuapp.com/api/book/${location.state}`;
    const header = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    };
    try {
      const res = await axios.get(url, header);
      setBookData(res.data);
      setLoading(false);
      console.log(res);
    } catch (error) {
      console.log(error);
    }
  }

  function formatDate(inputDate) {
    const date = new Date(inputDate);
    return `${date.getDate()}.${date.getMonth()}.${date.getFullYear()}`;
  }

  return (
    <div className='details'>
      <div className='image-container'>
        <img src={bookData.image} alt={bookData.name} />
      </div>

      <div className='book-data'>
        <div className='title-container'>
          <h1>{bookData.name}</h1>
        </div>

        <div className='body-details'>
          <div className='general-info'>
            <p className='author-details'>{bookData.author}</p>
            <p>
              Genre:{' '}
              <span className='span-details'>{!loading && bookData.genre.name}</span>
            </p>

            <p>
              Created on:{' '}
              <span className='span-details'>{formatDate(bookData.createOn)}</span>
            </p>
            <p>
              Updated on:{' '}
              <span className='span-details'>
                {formatDate(bookData.lastUpdateOn)}
              </span>
            </p>
          </div>
          <div className='description'>
            <h3>Short description</h3>
            <p>{ipsum1}</p>
            <p>{ipsum2}</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Details;
