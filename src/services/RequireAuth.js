import React from 'react';
import { Navigate } from 'react-router-dom';

function RequireAuth({ children }) {
  const token = localStorage.getItem('token');

  //fix to be routed to attempted location before login

  if (!token) {
    return <Navigate to='/login' replace />;
  }

  return children;
}

export default RequireAuth;
